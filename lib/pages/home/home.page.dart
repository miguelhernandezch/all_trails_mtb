import 'package:All_Trails_MTB/components/card/card.component.dart';
import 'package:All_Trails_MTB/components/card/card.resources.dart';
import 'package:All_Trails_MTB/pages/locations/locations.page.dart';
import 'package:All_Trails_MTB/pages/trail_list/trail_list.page.dart';
import 'package:All_Trails_MTB/pages/trail_detail/trail_detail.page.dart';
import 'package:flutter/material.dart';
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {

    final statesList = [
      {
        "id": "1",
        "title": "Nuevo Leon",
        "imageUrl": "https://www.nomada.news/wp-content/uploads/2019/01/7-cosas-que-no-sabes-del-cerro-de-la-silla.jpg",
      },
      {
        "id": "2",
        "title": "Coahuila",
        "imageUrl": "https://sp-images.summitpost.org/602077.JPG?auto=format&fit=max&h=800&ixlib=php-2.1.1&q=35&s=4a21bf07b5e451ea46b90483ddddd5e2",
      },
      {
        "id": "3",
        "title": "Tamaulipas",
        "imageUrl": "https://i.pinimg.com/originals/2f/70/e2/2f70e2c2f7e19b49b27ac253187f35cb.jpg",
      },
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text("All Trails"),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Center(
          child: ListView(
            children: <Widget>[
              for(var item in statesList ) TrailCard(item: item, url: "/trail_list", pageToGo: Locations(), cardType: cardTypes["state"])
            ],
          ),
        ),
      )
    );
  }
}
