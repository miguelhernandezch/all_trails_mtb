import 'package:All_Trails_MTB/components/card/card.component.dart';
import 'package:All_Trails_MTB/components/card/card.resources.dart';
import 'package:All_Trails_MTB/pages/trail_detail/trail_detail.page.dart';
import 'package:All_Trails_MTB/pages/trail_list/trail_list.page.dart';
import 'package:flutter/material.dart';

class Locations extends StatefulWidget {
  @override
  _LocationsState createState() => _LocationsState();
}

class _LocationsState extends State<Locations> {
  @override
  Widget build(BuildContext context) {

    final  routeParams = ModalRoute.of(context).settings.arguments;
    print(routeParams); // Trail data

    final trailList = [
      {
        "id": "1",
        "title": "Chipinque",
        "imageUrl":
        "https://media-cdn.tripadvisor.com/media/photo-s/02/5e/95/41/mirador-chipinque.jpg",
      },
      {
        "id": "2",
        "title": "Cola de Caballo",
        "imageUrl":
        "https://media.istockphoto.com/photos/woman-mountain-biking-on-forest-trails-picture-id1077209594?k=6&m=1077209594&s=170667a&w=0&h=CL13pYfVZ2ZS4sMivwiRxJr3Ir9OQ0Mrl1XPWQdLRJM=",
      },
      {
        "id": "3",
        "title": "La Huasteca",
        "imageUrl":
        "https://i.pinimg.com/originals/c1/ca/ad/c1caad8cb4c3a652fc320189a90f6dd6.jpg",
      },
      {
        "id": "3",
        "title": "Antenas de Vizcaya",
        "imageUrl":
        "https://s3.amazonaws.com/images.gearjunkie.com/uploads/2017/08/4Mountain-biking-the-yucatan.jpg",
      },
    ];

    return Scaffold(
        appBar: AppBar(
          title: Text("Lugares"),
        ),
        body: Padding(
          padding: EdgeInsets.all(16),
          child: Center(
            child: ListView(
              children: <Widget>[
                for (var item in trailList)
                  TrailCard(
                    item: item,
                    url: "/trail_detail",
                    pageToGo: TrailList(),
                    cardType: cardTypes["location"],
                  )
              ],
            ),
          ),
        ));
  }
}
