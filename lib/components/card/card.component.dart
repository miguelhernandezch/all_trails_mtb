import 'package:All_Trails_MTB/pages/trail_detail/trail_detail.page.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'card.resources.dart';

class TrailCard extends StatefulWidget {
  @override
  TrailCard({Key key, this.item, this.url, this.pageToGo, this.cardType})
      : super(key: key);
  final item;
  final String url;
  final Widget pageToGo;
  final String cardType;

  _TrailCardState createState() => _TrailCardState();
}

class _TrailCardState extends State<TrailCard> {
  @override
  Widget build(BuildContext context) {
    final cardConfig = {
      cardTypes["state"]: {
        "labelText": "Explora los increibles lugares que ${widget.item["title"]} tiene para ti.",
        "buttonText": "Ver Lugares"
      },
      cardTypes["location"]: {
        "labelText": "Explora las increibles trails de ${widget.item["title"]}",
        "buttonText": "Ver Trails",
      },
      cardTypes["trail"]: {
        "labelText": "Explora las increibles curvas de ${widget.item["title"]}",
        "buttonText": "Explorar Trail",
      },
    };

    final cardInfo = cardConfig[widget.cardType];

    final imageContainer = Container(
      height: 250,
      margin: EdgeInsets.only(bottom: 80),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          image: DecorationImage(
            image: NetworkImage(
              widget.item['imageUrl'],
            ),
            fit: BoxFit.cover,
          )),
    );

    final infoBody = Container(
      margin: EdgeInsets.only(left: 10, top: 10, right: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            widget.item["title"],
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              letterSpacing: 2,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Text(
              cardInfo["labelText"],
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.deepOrangeAccent,
                fontSize: 18,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            child: FlatButton.icon(
              onPressed: () {
                pushNewScreenWithRouteSettings(
                  context,
                  settings: RouteSettings(
                      name: widget.url,
                      arguments: {'stateSelected': widget.item}),
                  screen: widget.pageToGo,
                );
              },
              label: Text(cardInfo["buttonText"]),
              icon: Icon(
                Icons.directions_bike,
                size: 15,
              ),
            ),
          )
        ],
      ),
    );

    final infoContainer = Container(
      height: 140,
      width: 300,
      margin: EdgeInsets.only(
        top: 120,
        bottom: 20,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.black26, blurRadius: 10, offset: Offset(0, 3)),
        ],
        borderRadius: BorderRadius.circular(10),
      ),
      child: infoBody,
    );

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[imageContainer, infoContainer],
    );
  }
}
